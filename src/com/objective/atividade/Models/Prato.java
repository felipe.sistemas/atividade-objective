package com.objective.atividade.Models;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author Felipe Taynã 20/07/2020
 */
public class Prato {
    // Atributos
    private String nome; 
    private List<String> propriedades;
    
    public Prato(){
        //Cstr default..
    }
    
    /**
     * Método para instanciar um prato com o seu nome e propriedades específicas.
     * @param nome
     * @param propriedades
     */
    public Prato(String nome, List<String> propriedades){
        this.nome = nome;
        this.propriedades = propriedades;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<String> getPropriedades() {
        return propriedades;
    }

    public void setPropriedades(List<String> propriedades) {
        this.propriedades = propriedades;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.nome);
        hash = 29 * hash + Objects.hashCode(this.propriedades);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Prato other = (Prato) obj;
        
        return Objects.equals(this.nome, other.nome);
    }
    
    public String getPropriedadePrincipal(){
        return propriedades.get(0);
    }

    
    
}

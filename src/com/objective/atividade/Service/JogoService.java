package com.objective.atividade.Service;

import com.objective.atividade.Models.Prato;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Felipe Taynã 20/07/2020
 */
public class JogoService {

    private List<Prato> listaAuxiliar;
    private String propriedade;
    private final FiltroService filtroService;
    private final PratoService pratoService;

    public JogoService(FiltroService filtroService, PratoService pratoService) {
        this.filtroService = filtroService;
        this.pratoService = pratoService;
    }

    public boolean sairDoJogo() {
        int opcao = JOptionPane.showConfirmDialog(null, "Pense em um prato que você goste...", "Jogo Gourmet", JOptionPane.OK_CANCEL_OPTION);
        return opcao == JOptionPane.CLOSED_OPTION || opcao == JOptionPane.CANCEL_OPTION;
    }

    public void resetaJogo() {
        filtroService.resetaFiltro();
        listaAuxiliar = pratoService.getPratos();
    }

    public boolean listaPossuiPratos() {
        return !listaAuxiliar.isEmpty();
    }

    public void buscaPropriedade() {
        propriedade = filtroService.getPropriedadeNaoPresenteFiltro(listaAuxiliar);
    }

    public boolean verificaPropriedade() {
        return propriedade != null && !propriedade.isEmpty();
    }

    /**
     * Quando acabam as opções de propriedades dos pratos filtrados, tenta
     * advinhar.. Caso não acerte, adiciona um novo prato com as propriedades
     * filtradas e a inserida pelo usuário.
     */
    public void tentaAdvinhar() {
        int resposta = JOptionPane.showConfirmDialog(null, "O prato que você pensou é: ".concat(listaAuxiliar.get(0).getNome()), "Jogo Gourmet", JOptionPane.YES_NO_OPTION);

        if (resposta == JOptionPane.YES_OPTION) {
            mostraMensagemSucesso();
        } else {
            String nome = null, novaPropriedade = null;
            nome = JOptionPane.showInputDialog(null, "Qual prato você pensou: ", "Jogo Gourmet", JOptionPane.INFORMATION_MESSAGE);
            novaPropriedade = JOptionPane.showInputDialog(null, nome.concat(" é _______ mas ").concat(listaAuxiliar.get(0).getNome()).concat(" não."), "Jogo Gourmet", JOptionPane.INFORMATION_MESSAGE);

            filtroService.adicionaFiltro(novaPropriedade);
            Prato novo = new Prato(nome, filtroService.getFiltros());
            if (this.pratoService.validaPrato(novo)) {
                this.pratoService.adicionaPrato(novo);
            }
        }
    }

    public void mostraMensagemSucesso() {
        JOptionPane.showMessageDialog(null, "Acertei novamente!");
    }

    /**
     * Método verifica se a propriedade existe no prato desejado. Caso exista,
     * filtra por pratos que possuam propriedade. Caso contrário filtra por
     * pratos que não possuam.
     */
    public void continuaJogo() {
        if (JOptionPane.showConfirmDialog(null, "O prato que você pensou possui: ".concat(propriedade), "Jogo Gourmet", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            filtroService.adicionaFiltro(propriedade);
            listaAuxiliar = pratoService.filtraPratosPropriedades(filtroService.getFiltros(), listaAuxiliar);
        } else {
            listaAuxiliar = pratoService.removePratosPorPropriedade(propriedade, listaAuxiliar);
        }
    }

    public boolean ultimoElemento() {
        if (listaAuxiliar.size() == 1) {
            JOptionPane.showConfirmDialog(null, "O prato que você pensou possui: ".concat(propriedade), "Jogo Gourmet", JOptionPane.YES_NO_OPTION);
            tentaAdvinhar();
            return true;
        }
        return false;
    }
}

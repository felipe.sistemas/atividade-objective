package com.objective.atividade.Service;

import com.objective.atividade.Models.Prato;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Felipe Taynã 20/07/2020
 */
public class PratoService {

    private List<Prato> pratos;

    public PratoService() {
        this.pratos = new ArrayList<>();
        this.adicionaExemplos();
    }

    private void adicionaExemplos() {
        this.adicionaPrato(new Prato("Lasanha", Arrays.asList("Massa")));
        this.adicionaPrato(new Prato("Bolo de Chocolate", Arrays.asList("Chocolate")));
    }

    public void adicionaPrato(Prato prato) {
        this.pratos.add(prato);
    }

    public boolean existePrato(String nome) {
        return nome != null && !nome.isEmpty() ? this.pratos.stream().anyMatch(prato -> prato.getNome().equalsIgnoreCase(nome)) : false;
    }

    public List<Prato> getPratos() {
        return this.pratos;
    }

    public boolean validaPrato(Prato prato) {
        return prato != null && prato.getNome() != null;
    }

    public List<Prato> filtraPratosPropriedades(List<String> filtro, List<Prato> pratos) {
        return pratos.stream()
                .filter(p -> p.getPropriedades().containsAll(filtro))
                .collect(Collectors.toList());
    }
    
    public List<Prato> removePratosPorPropriedade(String filtro, List<Prato> pratos) {
        return pratos.stream()
                .filter(p -> !p.getPropriedades().contains(filtro))
                .collect(Collectors.toList());
    }

}

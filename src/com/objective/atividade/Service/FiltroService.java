package com.objective.atividade.Service;

import com.objective.atividade.Models.Prato;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Felipe Taynã 20/07/2020
 */
public class FiltroService {

    private List<String> filtros;

    public FiltroService() {
        filtros = new ArrayList<>();
    }

    public void adicionaFiltro(String nome) {
        if (!existeFiltro(nome)) {
            filtros.add(nome);
        }
    }

    public boolean existeFiltro(String nome) {
        return filtros.contains(nome);
    }

    public List<String> getFiltros() {
        return filtros;
    }

    /**
     * Método busca por Pratos que possuam alguma propriedade que não esteja nos
     * filtros.
     *
     * @param pratos
     * @return
     */
    public String getPropriedadeNaoPresenteFiltro(List<Prato> pratos) {
        for (Prato prato : pratos) {
            for (String propriedade : prato.getPropriedades()) {
                if (!filtros.contains(propriedade)) {
                    return propriedade;
                }
            }
        }

        return "";
    }

    public void resetaFiltro() {
        filtros = new ArrayList<>();
    }

}

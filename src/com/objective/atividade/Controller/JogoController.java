package com.objective.atividade.Controller;

import com.objective.atividade.Models.Prato;
import com.objective.atividade.Service.FiltroService;
import com.objective.atividade.Service.JogoService;
import com.objective.atividade.Service.PratoService;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Felipe Taynã da Silva
 */
public class JogoController {

    final private JogoService jogoService;

    public JogoController(PratoService pratoService, FiltroService filtroService) {
        this.jogoService = new JogoService(filtroService, pratoService);
    }

    public void iniciaJogo() {

        do {
            if (jogoService.sairDoJogo()) {
                break;
            }

            jogoService.resetaJogo();
            while (jogoService.listaPossuiPratos()) {

                jogoService.buscaPropriedade();

                if (!jogoService.verificaPropriedade()) {
                    jogoService.tentaAdvinhar();
                    break;
                }
                if (jogoService.ultimoElemento()) {
                    break;
                }
                jogoService.continuaJogo();

            }

        } while (true);

    }

}

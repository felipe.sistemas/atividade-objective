package com.objective.atividade;

import com.objective.atividade.Controller.JogoController;
import com.objective.atividade.Service.FiltroService;
import com.objective.atividade.Service.PratoService;

/**
 *
 * @author Felipe Taynã 20/07/2020
 */
public class Principal {

    public static void main(String[] args) {
        JogoController jogoController = new JogoController(new PratoService(), new FiltroService());
        jogoController.iniciaJogo();
    }

}
